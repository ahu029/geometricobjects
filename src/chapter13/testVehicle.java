package chapter13;

import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class testVehicle {
    public static void main(String[] args) {
        testVehicle test = new testVehicle();
        try {
            test.menuLoop();
        } catch(IOException e) {
            System.out.println("IO Exception!");
            System.exit(1);
        } catch(CloneNotSupportedException e) {
            System.out.println("CloneNotSupportedException");
            System.exit(1);
        }
    }

    private void menuLoop() throws IOException, CloneNotSupportedException {
        Scanner scan = new Scanner(System.in);
        ArrayList<Vehicle> arr=new ArrayList<Vehicle>();
        Vehicle vehicle;
        String name;
        Scanner input;

        //arr.add(new Car("Black", "Volvo", "1010-11", 2010, 85000, 163, 0));
        arr.add(new Car("Volvo","Red","YE6666",1996,85000,0,0,125));
        arr.add(new Car("toyota","blue","12345",2017,8000,0,0,200));
        arr.add(new Car("ford","white","65412",1999,85000,0,0,145));
        arr.add(new Bicycle("Diamant", "1111", "Blue", 1500, 21, 2011, 0,0));
        arr.add(new Bicycle("suzuk", "1011", "black", 1200, 20, 2002, 0,0));
        arr.add(new Bicycle("lifan", "1012", "red", 2000, 22, 2017, 0,0));
        while(true) {
            System.out.println("1...................................New car");
            System.out.println("2...............................New bicycle");
            System.out.println("3......................Find vehicle by name");
            System.out.println("4..............Show data about all vehicles");
            System.out.println("5.......Change direction of a given vehicle");
            System.out.println("6.........................RandomShit clone method");
            System.out.println("7..................RandomShit driveable interface");
            System.out.println("8..............................Exit program");
            System.out.print(".............................Your choice?");
            int choice = scan.nextInt();

            switch (choice) {
                case 1:
                    //add new car.
                    vehicle = new Car();
                    vehicle.setAllField();
                    arr.add(vehicle);
                    break;
                case 2:
                    //legg til en ny sykkel
                    vehicle = new Bicycle();
                    vehicle.setAllField();
                    arr.add(vehicle);

                    break;
                case 3:
                    //find the vehicle by name.
                    input = new Scanner(System.in);
                    System.out.println("Enter the a vehicle name: ");
                    name = input.nextLine();

                    for (int i = 0; i < arr.size(); i++) {
                        String tmpName = arr.get(i).getName();
                        if (name.equals(tmpName)) {
                            System.out.println(arr.get(i).toString());
                        }
                    }


                    break;
                case 4:
                    //show dat about all vehicle.
                    for (int i = 0; i < arr.size(); i++) {
                        System.out.println(arr.get(i).toString());
                    }
                    break;
                case 5:
                    // change the direction of a given car
                    System.out.println("Enter the a vehicle name: ");
                    input = new Scanner(System.in);
                    name = input.nextLine();

                    for (int i = 0; i < arr.size(); i++) {
                        String tmpName = arr.get(i).getName();
                        if (name.equals(tmpName)) {
                            System.out.println("Which direction do you want to move the vehicle: (1:left, 2:right)");
                            choice = input.nextInt();
                            switch (choice) {
                                case 1:
                                    System.out.print("How many degrees?: ");
                                    arr.get(i).turnLeft(input.nextInt());
                                    break;
                                case 2:
                                    System.out.println("How many degrees?: ");
                                    arr.get(i).turnRight(input.nextInt());
                                    break;
                            }
                        }
                    }
                    //test clone method.
                case 6:
                    vehicle = new Car("Volvo","Red","YE6666",1996,85000,0,0,125);
                    Vehicle clnVehicle = (Vehicle)vehicle.clone();
                    clnVehicle.setBuyingDate(new GregorianCalendar(2018, 4, 5));
                    System.out.println(vehicle.toString());
                    System.out.println(clnVehicle.toString());
                    break;
                    case 7:
                Vehicle car = new Car();
                Vehicle bike = new Bicycle();

                System.out.println("Car:");
                car.accelerate(20);
                car.accelerate(400);
                car.breaks(50);
                car.stop();

                System.out.println("Bike:");
                bike.accelerate(20);
                bike.accelerate(400);
                bike.breaks(50);
                bike.stop();
                break;
                case 8:
                    scan.close();
                    System.exit(0);
                default:
                    System.out.println("Wrong input!");
            }
            }}}



