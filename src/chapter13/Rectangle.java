package chapter13;

public class Rectangle extends GeometricObject {
    private double width;
    private double height;

    public Rectangle() {
    }
    public Rectangle(double width, double height) {
        this.width = width;
        this.height = height;
    }

    public double getWidth() {
        return width;
    }
    public void setWidth(double width) {
        this.width = width;
    }
    public double getHeight() {
        return height;
    }
    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public double getArea() {
        return width * height;
    }
    @Override
    public double getPerimeter() {
        return 2 * (width + height);
    }
    public String toString(){
        return  " width:"+ getWidth()+ " height : " + getHeight()+ " \n created on: "+ getDateCreated() + " \ncolor " + getColor()+ " \n filled " + isFilled();
    }
    public boolean equals(Object o) {
        if (this != o) {
            return false;
        }
        else {
            return true;
}
    }
}