package chapter13;


import java.io.PrintWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

public abstract class Vehicle  implements Comparable<Vehicle>, Cloneable, Driveable {
    private  String name;
    private  String color;
    private  String serialNumber;
    private  int model;
    private  int price;
    private  int  direction;
    private double speed;
    private int degree;
     //private final double MAX_SPEED_CAR = 0.5;
     //private final double MAX_SPEED_BIKE = 0.6;
    protected java.util.Scanner input;
    private Calendar buyingDate;
    Vehicle(){
        buyingDate = new GregorianCalendar(2020, 1, 10);
    }
    public Vehicle(String name, String color, String  serialNumber, int model, int price, int direction, double speed){
        buyingDate = new GregorianCalendar(1, 1, 1);
        this.color = color;
        this.direction = direction;
        this.name = name;
        this.price = price;
        this.model = model;
        this.serialNumber = serialNumber;
        speed = 0;
        buyingDate = new java.util.GregorianCalendar(model,  1, 1);
    }
    public void setAllField(){
        color = input.nextLine();
        name = input.nextLine();
        serialNumber = input.nextLine();
        model = input.nextInt();
        price = input.nextInt();
        direction = 0;
        speed = 0;
    }
    public abstract void turnLeft(int degrees);
    public abstract void turnRight(int degrees);


    public String getName(){
        return name;
    }
    public String getColor(){
        return  color;
    }
    public String getSerialNumber(){
        return serialNumber;
    }
    public int getModel(){
        return  model;
    }
    public int getPrice(){
        return price;
    }
    public int getDirection(){
        return direction;
    }
    public double getSpeed(){
        return speed;
    }
    protected void writeData(PrintWriter out) {
    }
    protected void readData(Scanner in) {
    }
    public GregorianCalendar getProductionDate(GregorianCalendar gregorianCalendar) {
        return (GregorianCalendar) buyingDate;
    }

    public void setName(String name){
        this.name = name;
    }
    public void setColor(String color){
        this.color = color;
    }
    public void setSerialNumber( String  serialNumber){
        this.serialNumber = serialNumber;
    }
    public void  setModel(int model){
        this.model =model;
    }
    public void  setPrice(int price){
        this.price = price;
    }
    public void setDirection(int direction){
        this.direction = direction;
    }
    public void setSpeed(double speed){
        this.speed = speed;
    }
    public void setBuyingDate(Calendar buyingDate) {
        this.buyingDate = buyingDate;
    }
    public void stop() {
        setSpeed(0);
        System.out.println("The vehicle has stopped!");
    }
    public Calendar getBuyingDate() {
        return buyingDate;
    }
    public String toString(){
        return  " production date: " + buyingDate.get(Calendar.YEAR)+"."
                + buyingDate.get(Calendar.MONTH)+"."+buyingDate.get(Calendar.DATE);
    }
    public int compareTo(Vehicle o) {
        if (this.price > o.price)
            return 1;
        else if (this.price < o.price)
            return -1;
        else
            return 0;
    }
        protected Object clone() throws CloneNotSupportedException {
            Vehicle cloneVehicle = (Vehicle) super.clone();
            cloneVehicle.buyingDate = (Calendar) buyingDate.clone();
            return cloneVehicle;
        }

}

