package chapter13;

public interface Driveable {
        double MAX_SPEED_CAR = 240.00;
        double MAX_SPEED_BIKE = 110.00;

        void accelerate(double speedFactor);

        void breaks(double speedFactor);

        void stop();
    }
