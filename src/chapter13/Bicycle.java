package chapter13;

import java.io.PrintWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class Bicycle extends Vehicle {
    private int gears;
    Bicycle(){
    }

    public Bicycle(String name, String serialNumber, String color, int price, int gears, int model, int direction, double speed) {
        this.gears = gears;
        setSerialNumber(serialNumber);
        setColor(color);
        setDirection(direction);
        setModel(model);
        setPrice(price);
        setSpeed(0);
        setName(name);
    }
    @Override
    public void setAllField() {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the bicyle's fields:");
        System.out.print("Colour: ");
        setColor(input.nextLine());
        System.out.print("Name: ");
        setName(input.nextLine());
        System.out.print("Serial Number: ");
        setSerialNumber(input.nextLine());
        System.out.print("Model year: ");
        setModel(input.nextInt());
        System.out.print("Price: ");
        setPrice(input.nextInt());
        System.out.print("Gear: ");
        gears = input.nextInt();
        setDirection(0);
        setSpeed(0);
    }

    @Override
    public void turnLeft(int degrees) {
        System.out.println("The bicycle turned " + degrees + " degrees to the left");
    }
    @Override
    public void accelerate(double speedFactor) {
        if (getSpeed()==0)
            setSpeed(0.3*speedFactor);
        else setSpeed(getSpeed()*0.5*speedFactor);

        if (getSpeed()>=MAX_SPEED_BIKE)
            setSpeed(MAX_SPEED_BIKE);

        if (getSpeed() == MAX_SPEED_BIKE){
            System.out.println("The bike has reached its max speed of " + MAX_SPEED_BIKE);
        }else System.out.println("The bike is accelerating and a new speed is " + getSpeed());
    }

    @Override
    public void breaks(double speedFactor) {
        setSpeed(getSpeed()/(speedFactor*0.5));
        System.out.println("The bike is breaking, the new speed is " + getSpeed());}

    @Override
    public void turnRight(int degrees) {
        System.out.println("The bicycle turned " + degrees + " degrees to the right");
    }

    public int getGears() {
        return gears;
    }
    @Override
    public void writeData(PrintWriter out) {
        super.writeData(out);
    }

    @Override
    public void readData(Scanner in) {
        super.readData(in);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
    public int getProductionDate(int productionDate){
        return productionDate;
    }
    public String toString(){
        return  " production date: " + getBuyingDate().get(Calendar.YEAR)+"."
                + getBuyingDate().get(Calendar.MONTH)+"."+getBuyingDate().get(Calendar.DATE);
    }
}

