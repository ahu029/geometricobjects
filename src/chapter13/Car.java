package chapter13;

import java.io.PrintWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class Car extends Vehicle {
    private int power;
    Car(){
    }
    public Car( String name, String serialNumber, String color, int price, int power, int model, int direction, double speed){
        this.power = power;
        setSerialNumber(serialNumber);
        setColor(color);
        setDirection(direction);
        setModel(model);
        setPrice(price);
        setSpeed(0);
        setName(name);
    }
    @Override

    public void setAllField(){
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the car's fields:");
        System.out.print("Colour: ");
        setColor(input.nextLine());
        System.out.print("Name: ");
        setName(input.nextLine());
        System.out.print("SerialNumber: ");
        setSerialNumber(input.nextLine());
        System.out.print("Model: ");
        setModel(input.nextInt());
        System.out.print("Price: ");
        setPrice(input.nextInt());
        System.out.print("Power: ");
        power = input.nextInt();
        setDirection(0);
        setSpeed(0);
    }
    @Override
    public void turnLeft(int degrees){
        if (0 <= degrees && degrees <= 360){
            setDirection(getDirection() - degrees);
            if (getDirection() < 0)
                setDirection(getDirection()-360);
    }
    }
    @Override
    public void turnRight(int degrees){
            if (0 <= degrees && degrees <= 360){
                setDirection(getDirection() + degrees);
                if (getDirection() >= 360)
                    setDirection(getDirection()+360);
            }
    }
    @Override
    public void accelerate(double speedFactor) {
        if (getSpeed()== 0)
            setSpeed(0.5*speedFactor);
        else
            setSpeed(getSpeed()*speedFactor);

        if (getSpeed()>=MAX_SPEED_CAR)
            setSpeed(MAX_SPEED_CAR);

        if (getSpeed() == MAX_SPEED_CAR){
            System.out.println("The car has reached its max speed of " + MAX_SPEED_CAR);
        }else System.out.println("The car is accelerating and  the new speed is " + getSpeed());
    }

    @Override
    public void breaks(double speedFactor) {
        setSpeed(getSpeed()/speedFactor);
        System.out.println("The car is breaking, the new speed is " + getSpeed());
    }

    @Override
    public void writeData(PrintWriter out) {
        super.writeData(out);
    }

    @Override
    public void readData(Scanner in) {
        super.readData(in);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
    public void setPower( int power){
        this.power = power;
    }
    @Override
      public String toString(){
        return " production date: " + getBuyingDate().get(Calendar.YEAR)+"."
                + getBuyingDate().get(Calendar.MONTH)+"."+getBuyingDate().get(Calendar.DATE);
    }
}




